<?php
require_once 'bootstrap.php';

/*if(!isUserLoggedIn() || !isset($_GET["action"]) || ($_GET["action"]!=1 && $_GET["action"]!=2 && $_GET["action"]!=3) || ($_GET["action"]!=1 && !isset($_GET["id"]))){
    header("location: login.php");
}*/

if(isset($_SESSION["email"])){
    $templateParams["notifiche"] = $dbh->getNotifications($_SESSION["email"]);
}

if($_GET["action"]!=1){
    $risultato = $dbh->getEventByIdAndOrganizer($_GET["id"], $_SESSION["idorganizzatore"]);
    if(count($risultato)==0){
        $templateParams["evento"] = null;
    }
    else{
        $templateParams["evento"] = $risultato[0];
        $templateParams["evento"]["categorie"] = explode(",", $templateParams["evento"]["categorie"]);
    }
}
else{
    $templateParams["evento"] = getEmptyEvent();
}




$templateParams["titolo"] = "Blog TW - Gestisci Eventi";
$templateParams["nome"] = "login/organizzatore/organizzatore-form.php";
$templateParams["categorie"] = $dbh->getCategories();
$templateParams["eventicasuali"] = $dbh->getRandomEvents(2);

$templateParams["azione"] = $_GET["action"];

require 'template/base.php';
?>