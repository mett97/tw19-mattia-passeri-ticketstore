<?php
require_once 'bootstrap.php';

//Base Template
$templateParams["titolo"] = "TicketStore - Eventi";
$templateParams["nome"] = "home.php";
$templateParams["categorie"] = $dbh->getCategories();
$templateParams["eventicasuali"] = $dbh->getRandomEvents(2);
if(isset($_SESSION["email"])){
    $templateParams["notifiche"] = $dbh->getNotifications($_SESSION["email"]);
}
//Eventi Categoria Template
$idcategoria = -1;
if(isset($_GET["id"])){
    $idcategoria = $_GET["id"];
}
$nomecategoria = $dbh->getCategoryById($idcategoria);
if(count($nomecategoria)>0){
    $templateParams["titolo"] = "Blog TW - Eventi ".$nomecategoria[0]["nomecategoria"];
    $templateParams["titolo_pagina"] = "Eventi della categoria ".$nomecategoria[0]["nomecategoria"];
    $templateParams["eventi"] = $dbh->getEventByCategory($idcategoria);
}
else{
    $templateParams["titolo_pagina"] = "Categoria non trovata"; 
    $templateParams["eventi"] = array();   
}

require 'template/base.php';
?>