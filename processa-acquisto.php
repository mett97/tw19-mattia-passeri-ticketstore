<?php
require_once 'bootstrap.php';

if (isset($_SESSION["email"])) {
    $templateParams["notifiche"] = $dbh->getNotifications($_SESSION["email"]);
}

if ($_POST["action"] == 1) {
    //acquisto
    $idcliente = $_SESSION["idcliente"];
    $idevento = $_POST["idevento"];
    $nomeevento = $dbh->getEventById($idevento)[0]["titoloevento"];
    $num = $_POST["numbiglietti"];
    $max_biglietti = $dbh->numTicketsEvent($idevento)[0]["maxbiglietti"];
    $num_biglietti = $dbh->ticketsSoldEvent($idevento);
    if (($num_biglietti + $num) <= $max_biglietti) {
        $dbh->buyEvent($idcliente, $idevento, $num);
        $destinatario = $dbh->getOrganizer($idevento)[0]["email"];
        $dbh->sendNotification($_SESSION["email"], $destinatario, "Un cliente ha acquistato " . $num . " biglietti per il tuo evento ".$nomeevento);
        $dbh->sendNotification($destinatario, $_SESSION["email"], "Hai acquistato " . $num . " biglietti per l'evento ".$nomeevento);
        $msg = "Acquisto effettuato correttamente!";
        if ($dbh->ticketsSoldEvent($idevento) == $max_biglietti) {
            $dbh->sendNotification($destinatario, $destinatario, "Evento " . $nomeevento . " sold out!");
            $ads = $dbh->getAdmins();
            foreach ($ads as $ad) {
                $dbh->sendNotification($destinatario, $ad["email"], "Evento " . $nomeevento . " sold out!");
            }
        }
    } else {
        if (($max_biglietti - $num_biglietti)>0){
            $tot = ($max_biglietti - $num_biglietti);
            $msg = "Impossibile acquistare questi biglietti! Puoi acquistarne solo ".$tot;
        } else {
            $msg = "Impossibile acquistare questi biglietti! Evento sold out";
        }
    }
    header("location: login.php?formmsg=" . $msg);
}
?>