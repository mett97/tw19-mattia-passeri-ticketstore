        <div class="container mt-3">
            <section>
                <div class="pl-3 pr-3 mb-3 d-flex justify-content-between">
                    <h2>Notifiche</h2>
                    <?php if (isset($templateParams["formmsg"])) : ?>
                        <p><?php echo $templateParams["formmsg"]; ?></p>
                    <?php endif; ?>
                </div>
                    <div class="row pl-3 pr-3">
                            <?php foreach ($templateParams["notifiche"] as $notifica) { ?>
                                <div class="col-12 .col-sm-6 .col-lg-8" style="padding-bottom:10px;">
                                    <article class="card h-100">
                                        <div class="card-header">
                                            <h5 class="mb-0 text-center">mittente: <?php echo $notifica["mittente"]; ?> </h5>
                                        </div>
                                        <div class="card-body text-center vcenter" style="padding-bottom:10px;">
                                            <div class="row justify-content-md-center">
                                                <p style="margin:10px"> <?php echo $notifica["messaggio"]; ?> </p>
                                            </div>
                                            <form class="d-flex" action="notifiche.php" method="post">
                                                <input type="hidden" name="id" value="<?php echo $notifica["id"]; ?>" />
                                                <?php if($notifica["visto"]==0): ?>
                                                <input type="submit" class="p-2 btn btn-link" value="Visto" name="visto">
                                                <?php endif; ?>
                                                <input type="submit" class="ml-auto p-2 btn btn-link" value="Cancella" name="cancella">
                                            </form>
                                        </div>
                                    </article>
                                </div>
                            <?php } ?>
            </section>
        </div>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="js/home-view.js"></script>