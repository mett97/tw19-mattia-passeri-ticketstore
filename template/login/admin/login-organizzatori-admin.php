<div class="container mt-3">
    <section>
        <div class="pl-3 pr-3 mb-3 d-flex justify-content-between">
            <h2>Organizzatori</h2>
            <?php if (isset($templateParams["formmsg"])) : ?>
                <p><?php echo $templateParams["formmsg"]; ?></p>
            <?php endif; ?>
        </div>
        <?php if (isset($templateParams["organizzatori"])) : ?>
            <div class="container justify-content-between">
                <?php if (isset($templateParams["titolo_pagina"])) : ?>
                    <h2 class="pb-3"><?php echo $templateParams["titolo_pagina"]; ?></h2>
                <?php endif; ?>
                <?php
                $i = 0;
                while ($i < count($templateParams["organizzatori"])) :
                ?>
                    <div class="row mb-2 justify-content-center">
                        <?php foreach (array_slice($templateParams["organizzatori"], $i, $i + 2) as $organizzatore) : ?>
                            <div class="col-md-6">
                                <article class="home-article row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative" style="max-height: 500px;">
                                    <form action="processa-organizzatore.php" style="width: 100%;" method="POST" enctype="multipart/form-data">
                                        <div class="col p-4 d-flex flex-column position-static">
                                            <h3 class="mb-1"><?php echo $organizzatore["nome"]; ?></h3>
                                            <div class="card-text mb-1" style="word-wrap: break-word;">
                                                <ul>
                                                    <li><?php echo $organizzatore["email"]; ?></li>
                                                    <li><?php echo $organizzatore["indirizzo"]; ?>, <?php echo $organizzatore["città"]; ?></li>
                                                    <li><?php echo $organizzatore["descrizione"]; ?></li>
                                                </ul>
                                            </div>
                                            <div class="d-flex flex-row-reverse justify-content-between">
                                                <input type="submit" class="btn btn-link" name="submit" value="Cancella" />
                                            </div>
                                            <input type="hidden" name="idorganizzatore" value="<?php echo $organizzatore["idorganizzatore"]; ?>" />
                                            <input type="hidden" name="action" value="3" />
                                        </div>
                                    </form>
                                </article>
                                <?php
                                $i += 1;
                                ?>
                            </div>
                        <?php
                        endforeach;
                        ?>
                    </div>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>
    </section>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="js/home-view.js"></script>