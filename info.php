<?php
require_once 'bootstrap.php';

//Base Template
$templateParams["titolo"] = "TicketStore - Info";
$templateParams["nome"] = "informazioni.php";
$templateParams["titolo_pagina"] = "Informazioni";
if(isset($_SESSION["email"])){
    $templateParams["notifiche"] = $dbh->getNotifications($_SESSION["email"]);
}

require 'template/base.php';
?>