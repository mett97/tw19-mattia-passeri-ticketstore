-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema ticketstore_db
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Table `ticketstore_db`.`admin`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `ticketstore_db`.`admin` (
  `nome` varchar(40) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL
) ENGINE=InnoDB;

INSERT INTO `admin` (`nome`, `email`, `password`) VALUES
('mattia', 'mattia@ticketstore.com', '21dda7961d1db06b84ec3bf0caaa96e0f23ea0f89ac8e0aa5a9c2c549dbc3575');

-- -----------------------------------------------------
-- Table `ticketstore_db`.`organizzatore`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `ticketstore_db`.`organizzatore` (
  `idorganizzatore` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  `email` VARCHAR(100) NOT NULL,
  `password` VARCHAR(512) NOT NULL,
  `città` varchar(40) NOT NULL,
  `indirizzo` varchar(60) NOT NULL,
  `descrizione` varchar(300) NOT NULL,
  `attivo` TINYINT NULL DEFAULT 0,
  PRIMARY KEY (`idorganizzatore`))
ENGINE = InnoDB;

INSERT INTO `organizzatore` (`idorganizzatore`, `nome`, `email`, `password`, `città`, `indirizzo`, `descrizione`, `attivo`) VALUES
(1, 'Grandi Concerti', 'grandiconcerti@ticketstore.com', '7267bbbf498a29d18d754de5dc2c45bc8e3803e257d76f681a2d9dd9ea322b1f', 'Roma', 'Via Lince 9', 'Organizziamo emozionanti concerti delle migliori star del momento, sia in Italia che fuori.', 1),
(2, 'Mostre&Cinema', 'mostrecinema@ticketstore.com', '91b8a74c7a9664ae13da287a2f9abdb6edc16324c27f528b655ef1896465c19a', 'Rimini', 'Via Daqui 20', 'Affermato organizzatore di mostre artistiche e proiezioni cinematografiche, operante da anni nel settore.', 1);

ALTER TABLE `organizzatore`
  MODIFY `idorganizzatore` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

-- -----------------------------------------------------
-- Table `ticketstore_db`.`evento`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `ticketstore_db`.`evento` (
  `idevento` INT NOT NULL AUTO_INCREMENT,
  `titoloevento` VARCHAR(100) NOT NULL,
  `testoevento` MEDIUMTEXT NOT NULL,
  `dataevento` DATE NOT NULL,
  `luogoevento` VARCHAR(100) NOT NULL,
  `anteprimaevento` VARCHAR(300) NOT NULL,
  `imgevento` VARCHAR(100) NOT NULL,
  `maxbiglietti` INT(11) NOT NULL,
  `organizzatore` INT NOT NULL,
  PRIMARY KEY (`idevento`),
  INDEX `fk_evento_organizzatore_idx` (`organizzatore` ASC),
  CONSTRAINT `fk_evento_organizzatore`
    FOREIGN KEY (`organizzatore`)
    REFERENCES `ticketstore_db`.`organizzatore` (`idorganizzatore`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

INSERT INTO `evento` (`idevento`, `titoloevento`, `testoevento`, `dataevento`, `luogoevento`, `anteprimaevento`, `imgevento`, `maxbiglietti`, `organizzatore`) VALUES
(1, 'VASCO ROSSI – NON STOP LIVE FESTIVAL', 'Il 2020 sarà l’anno dei Festival rock per Vasco Rossi che, per l’estate prossima, ha scelto di fare una pausa dagli stadi e di esibirsi nei più importanti Festival Rock della penisola. Il Blasco tornerà ad Imola per concludere il Tour del Non Stop Live Festival con un concerto imperdibile: il 26 giugno all’Autodromo Internazionale Enzo e Dino Ferrari di Imola,  si esibirà davanti al suo pubblico proprio dove nel 1998 fece il tutto esaurito con 130.000 fan durante l’indimenticabile prima edizione dell’Heineken Jammin’ Festival.', '2020-06-26', 'Imola', 'Vasco Non Stop Live Festival! Vasco torna live nel 2020 per 5 concerti nei 4 festival rock più importanti dell’estate!', 'vasco.jpg', 10, 1),
(2, 'FELLINI100. Genio Immortale. La mostra', 'Il 20 gennaio del 1920 nasceva a Rimini Federico Fellini, il Maestro del cinema mondiale. Se l’Italia è diventata, per tutto il mondo, il paese della "dolce vita" lo si deve al suo sguardo unico e inconfondibile. Pochissimi artisti sono riusciti a rappresentare l’intera storia del nostro Paese come ha fatto Fellini. Un artista che attraverso il cinema è riuscito a inventare un mondo intero, creando un immaginario capace non solo di raccontare la propria generazione ma anche di entrare in contatto con quelle successive. Fellini ci ha mostrato come, viaggiando a ritroso nel tempo, si possono trovare magici suggerimenti per comprendere il presente. "Tutto si immagina" non è solo una celebre espressione del regista riminese, genio immortale, ma la chiave di volta per fotografarne l’eredità artistica e creativa attuale e senza tempo. Questa grande mostra, che sarà ospitata il prossimo aprile 2020 a Roma (Palazzo Venezia) per poi varcare i confini nazionali con esposizioni a Los Angeles, Mosca e Berlino, inaugura le iniziative dedicate al Maestro nel centenario della nascita. Rappresenta infatti l’occasione per riportare in primo piano memorie, emozioni, fotogrammi, scene, suggestioni provenienti da quel mondo straordinario capace di dirci tutta la verità su noi stessi con l’irresistibile fascino universale del sogno.', '2020-03-22', 'Roma', 'Questa grande mostra rappresenta l’occasione per riportare in primo piano memorie, emozioni, fotogrammi, scene, suggestioni provenienti dal mondo straordinario del maestro Fellini.', 'fellini.jpg', 1000, 2),
(3, 'Tolo Tolo', 'Stavolta Zalone vestirà i panni di un comico napoletano minacciato da un boss malavitoso e volerà fino in Kenya, più precisamente nella città di Malindi, località costiera di Watamu. Nel film Checco sarà affiancato anche da un carabiniere, il quale diventa suo amico e compagno di peripezie.', '2020-02-10', 'Rimini', 'Stavolta Zalone vestirà i panni di un comico napoletano minacciato da un boss malavitoso e volerà fino in Kenya, più precisamente nella città di Malindi, località costiera di Watamu.', 'tolo-tolo.jpg', 10, 2),
(4, 'Imagine Dragons Firenze 2020', 'Imagine Dragons in Italia in concerto a Firenze nel 2020 – La band che ha conquistato certificazioni multi-Platino e vinto un Grammy Award, si esibirà live nel nostro Paese il 2 giugno 2020 alla Visarno Arena di Firenze. Disponibile servizio Bus Concerto Imagine Dragons. Quella in Italia sarà l’unica tappa Europea del tour della band capitanata da Dan Reynolds. Sul palco, oltre ai loro più grandi successi, gli Imagine Dragons presenteranno dal vivo i brani contenuti nel quarto album “ORIGINS”. “ORIGINS” è stato concepito come un album gemello di “EVOLVE”, uscito 16 mesi prima, continuando le esplorazioni sonore del precedente lavoro.', '2020-06-02', 'Firenze', 'Imagine Dragons in Italia in concerto a Firenze nel 2020 – La band che ha conquistato certificazioni multi-Platino e vinto un Grammy Award, si esibirà live nel nostro Paese il 2 giugno 2020 alla Visarno Arena di Firenze.', 'imagined.jpg', 100, 1);

ALTER TABLE `evento`
  MODIFY `idevento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

-- -----------------------------------------------------
-- Table `ticketstore_db`.`categoria`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `ticketstore_db`.`categoria` (
  `idcategoria` INT NOT NULL AUTO_INCREMENT,
  `nomecategoria` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`idcategoria`))
ENGINE = InnoDB;

INSERT INTO `categoria` (`idcategoria`, `nomecategoria`) VALUES
(1, 'Arte'),
(2, 'Musica'),
(3, 'Spettacolo'),
(4, 'Eventi'),
(5, 'Cinema');

ALTER TABLE `categoria`
  MODIFY `idcategoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

-- -----------------------------------------------------
-- Table `ticketstore_db`.`evento_ha_categoria`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `ticketstore_db`.`evento_ha_categoria` (
  `evento` INT NOT NULL,
  `categoria` INT NOT NULL,
  PRIMARY KEY (`evento`, `categoria`),
  INDEX `fk_evento_has_categoria_categoria1_idx` (`categoria` ASC),
  INDEX `fk_evento_has_categoria_evento1_idx` (`evento` ASC),
  CONSTRAINT `fk_evento_has_categoria_evento1`
    FOREIGN KEY (`evento`)
    REFERENCES `ticketstore_db`.`evento` (`idevento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_evento_has_categoria_categoria1`
    FOREIGN KEY (`categoria`)
    REFERENCES `ticketstore_db`.`categoria` (`idcategoria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

INSERT INTO `evento_ha_categoria` (`evento`, `categoria`) VALUES
(1, 2),
(1, 4),
(2, 1),
(2, 5),
(3, 5),
(4, 2),
(4, 3);

-- --------------------------------------------------------
-- Table structure for table `cliente`
-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `ticketstore_db`.`cliente` (
  `idcliente` INT NOT NULL AUTO_INCREMENT,
  `nome` char(15) NOT NULL,
  `cognome` char(20) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `città` varchar(40) NOT NULL,
  `attivo` TINYINT NULL DEFAULT 0,
  PRIMARY KEY (`idcliente`)
) ENGINE=InnoDB;

INSERT INTO `cliente` (`idcliente`, `nome`, `cognome`, `email`, `password`, `città`, `attivo`) VALUES
(1, 'Giuseppe', 'Zucchi', 'giuseppez@ticketstore.com', '524f04e8e0a4f1fbbc5d5ea30c95b3b1cf664eb600ac9d06035e3b787be0bf04', 'Cesenatico', 1),
(2, 'Mariella', 'Pecoraro', 'mariellapecoraro@ticketstore.com', 'd4398c6054ad861c3225a95474b595e9973e8dc1dd9482b0ed4a16c5fa63a396', 'Taormina', 1),
(3, 'Luca', 'Bianchi', 'luca.bianchi@ticketstore.com', '0966f7e686fc70db08ff162d7dfc3a65dbdcffade96015831018b7f0268dccbc', 'Rimini', 1);

ALTER TABLE `cliente`
  MODIFY `idcliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

-- -----------------------------------------------------
-- Table `ticketstore_db`.`cliente_partecipa`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `ticketstore_db`.`cliente_partecipa` (
  `evento` INT NOT NULL,
  `cliente` INT NOT NULL,
  `numbiglietti` INT NOT NULL,
  PRIMARY KEY (`evento`, `cliente`),
  INDEX `fk_cliente_partecipa_cliente1_idx` (`cliente` ASC),
  INDEX `fk_cliente_partecipa_evento1_idx` (`evento` ASC),
  CONSTRAINT `fk_cliente_partecipa_evento1`
    FOREIGN KEY (`evento`)
    REFERENCES `ticketstore_db`.`evento` (`idevento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cliente_partecipa_cliente1`
    FOREIGN KEY (`cliente`)
    REFERENCES `ticketstore_db`.`cliente` (`idcliente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

INSERT INTO `cliente_partecipa` (`evento`, `cliente`, `numbiglietti`) VALUES
(1, 1, 3),
(1, 2, 1),
(2, 1, 1),
(2, 3, 2),
(3, 1, 3),
(3, 3, 1),
(4, 2, 2);

-- -----------------------------------------------------
-- Table `ticketstore_db`.`notifica`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `ticketstore_db`.`notifica` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `destinatario` varchar(128) NOT NULL,    -- e_mail fornitore/ cliente/ admin
  `messaggio` varchar(255) NOT NULL,
  `visto` tinyint(1) NOT NULL,
  `mittente` varchar(128) NOT NULL,        -- e_mail fornitore/ cliente/ admin
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

INSERT INTO `notifica` (`id`,`destinatario`,`messaggio`,`visto`,`mittente`) VALUES
(2,'ginopino@ticketstore.com', 'Un cliente ha effettuato un acquisto', 0, 'giuseppez@ticketstore.com'),
(3,'giuseppez@ticketstore.com', 'Acquisto effettuato', 0, 'grandiconcerti@ticketstore.com'),
(4,'mattia@ticketstore.com', 'Un cliente si è registrato', 0, 'mariellapecoraro@ticketstore.it'),
(5,'mattia@ticketstore.com', 'Un cliente si è registrato', 0, 'luca.bianchi@ticketstore.it'),
(6,'mattia@ticketstore.com', 'Un organizzatore si è registrato', 0, 'grandiconcerti@ticketstore.com');

ALTER TABLE `notifica`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;







SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;