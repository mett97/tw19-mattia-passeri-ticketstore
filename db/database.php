<?php
class DatabaseHelper
{
    private $db;

    public function __construct($servername, $username, $password, $dbname)
    {
        $this->db = new mysqli($servername, $username, $password, $dbname);
        if ($this->db->connect_error) {
            die("Connection failed: " . $this->db->connect_error);
        }
    }

    public function getCoverEvent()
    {
        $n = 1;
        $stmt = $this->db->prepare("SELECT idevento, titoloevento, imgevento FROM evento ORDER BY RAND() LIMIT ?");
        $stmt->bind_param('i', $n);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getRandomEvents($n)
    {
        $stmt = $this->db->prepare("SELECT idevento, titoloevento, imgevento, anteprimaevento FROM evento ORDER BY RAND() LIMIT ?");
        $stmt->bind_param('i', $n);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getCategories()
    {
        $stmt = $this->db->prepare("SELECT * FROM categoria");
        //var_dump($this->db->error);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getCategoryById($idcategory)
    {
        $stmt = $this->db->prepare("SELECT nomecategoria FROM categoria WHERE idcategoria=?");
        $stmt->bind_param('i', $idcategory);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEvents($n = -1)
    {
        $query = "SELECT idevento, luogoevento, titoloevento, imgevento, anteprimaevento, dataevento, nome FROM evento, organizzatore WHERE organizzatore=idorganizzatore ORDER BY dataevento DESC";
        if ($n > 0) {
            $query .= " LIMIT ?";
        }
        $stmt = $this->db->prepare($query);
        if ($n > 0) {
            $stmt->bind_param('i', $n);
        }
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function ticketsSoldEvent($idevento){
        $query = "SELECT numbiglietti FROM cliente_partecipa WHERE evento=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $idevento);
        $stmt->execute();
        $result = $stmt->get_result();
        $res = $result->fetch_all(MYSQLI_ASSOC);
        if (count($res)>0){
            $num = 0;
            foreach($res as $r) {
                $num += $r["numbiglietti"];
            }
            return $num;
        }
        return 0;
    }

    public function numTicketsEvent($idevento) {
        $query = "SELECT maxbiglietti FROM evento WHERE idevento=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $idevento);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventById($id)
    {
        $query = "SELECT idevento, luogoevento,  titoloevento, imgevento, maxbiglietti, testoevento, dataevento, nome, idorganizzatore FROM evento, organizzatore WHERE idevento=? AND organizzatore=idorganizzatore";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getCategoryByEvent($idevento)
    {
        $query = "SELECT categoria FROM evento_ha_categoria WHERE evento=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $idevento);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventByCategory($idcategory)
    {
        $query = "SELECT idevento, luogoevento, titoloevento, imgevento, maxbiglietti, anteprimaevento, dataevento, nome FROM evento, organizzatore, evento_ha_categoria WHERE categoria=? AND organizzatore=idorganizzatore AND idevento=evento";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $idcategory);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventByIdAndOrganizer($id, $idauthor)
    {
        $query = "SELECT idevento, luogoevento, anteprimaevento, titoloevento, maxbiglietti, imgevento, testoevento, dataevento, (SELECT GROUP_CONCAT(categoria) FROM evento_ha_categoria WHERE evento=idevento GROUP BY evento) as categorie FROM evento WHERE idevento=? AND organizzatore=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii', $id, $idauthor);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventByOrganizerId($id)
    {
        $query = "SELECT idevento, luogoevento, anteprimaevento, titoloevento, maxbiglietti, imgevento, testoevento, dataevento FROM evento WHERE organizzatore=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventByClientId($id)
    {
        $query = "SELECT idevento, luogoevento, titoloevento, imgevento, maxbiglietti, anteprimaevento, dataevento FROM evento, cliente_partecipa WHERE cliente=? AND idevento=evento";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function insertOrganizer($nome, $email, $password, $città, $indirizzo, $descrizione)
    {
        $query = "INSERT INTO organizzatore (nome, email, password, città, indirizzo, descrizione, attivo) VALUES (?, ?, ?, ?, ?, ?, 1)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ssssss', $nome, $email, $password, $città, $indirizzo, $descrizione);
        $stmt->execute();

        return $stmt->insert_id;
    }

    public function insertUser($nome, $cognome, $email, $password, $città)
    {
        $query = "INSERT INTO cliente (nome, cognome, email, password, città, attivo) VALUES (?, ?, ?, ?, ?, 1)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sssss', $nome, $cognome, $email, $password, $città);
        $stmt->execute();

        return $stmt->insert_id;
    }

    public function organizerActive($idorganizzatore)
    {
        $query = "SELECT attivo FROM organizzatore WHERE idorganizzatore=? ";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $idorganizzatore);
        $stmt->execute();
        $result = $stmt->get_result();
        $res = $result->fetch_all(MYSQLI_ASSOC);
        if ($res[0]["attivo"]==1) {
            return true;
        } else {
            return false;
        }
    }

    public function insertEvent($titoloevento, $testoevento, $anteprimaevento, $luogoevento, $dataevento, $imgevento, $organizzatore, $maxbiglietti)
    {
        $query = "INSERT INTO evento (titoloevento, testoevento, anteprimaevento, dataevento, luogoevento, imgevento, maxbiglietti, organizzatore) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ssssssii', $titoloevento, $testoevento, $anteprimaevento, $dataevento, $luogoevento, $imgevento, $maxbiglietti, $organizzatore);
        $stmt->execute();

        return $stmt->insert_id;
    }

    public function updateEventOfOrganizer($idevento, $titoloevento, $testoevento, $anteprimaevento, $luogoevento, $dataevento, $imgevento, $organizzatore, $maxbiglietti)
    {
        $query = "UPDATE evento SET titoloevento = ?, testoevento = ?, anteprimaevento = ?, imgevento = ?, luogoevento = ?, dataevento = ?, maxbiglietti = ? WHERE idevento = ? AND organizzatore = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ssssssiii', $titoloevento, $testoevento, $anteprimaevento, $imgevento, $luogoevento, $dataevento, $maxbiglietti, $idevento, $organizzatore);
        var_dump($stmt->error);
        return $stmt->execute();
    }

    public function deleteEvent($idevento)
    {
        $query = "DELETE FROM evento_ha_categoria WHERE evento = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $idevento);
        $stmt->execute();
        var_dump($stmt->error);
        $query = "DELETE FROM cliente_partecipa WHERE evento = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $idevento);
        $stmt->execute();
        var_dump($stmt->error);
        $query = "DELETE FROM evento WHERE idevento = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $idevento);
        $stmt->execute();
        var_dump($stmt->error);
        return true;
    }

    public function insertCategoryOfEvent($evento, $categoria)
    {
        $query = "INSERT INTO evento_ha_categoria (evento, categoria) VALUES (?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii', $evento, $categoria);
        return $stmt->execute();
    }

    public function deleteCategoryOfEvent($evento, $categoria)
    {
        $query = "DELETE FROM evento_ha_categoria WHERE evento = ? AND categoria = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii', $evento, $categoria);
        return $stmt->execute();
    }

    public function deleteCategoriesOfEvent($evento)
    {
        $query = "DELETE FROM evento_ha_categoria WHERE evento = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $evento);
        return $stmt->execute();
    }

    public function getOrganizers()
    {
        //$query = "SELECT idorganizzatore, email, nome, città, indirizzo, descrizione, GROUP_CONCAT(DISTINCT nomecategoria) as argomenti FROM categoria, evento, organizzatore, evento_ha_categoria WHERE idevento=evento AND categoria=idcategoria AND organizzatore=idorganizzatore AND attivo=1 GROUP BY email, nome";
        $query = "SELECT idorganizzatore, email, nome, città, indirizzo, descrizione FROM  organizzatore  WHERE attivo=1";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getAdmins()
    {
        $query = "SELECT email FROM admin";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getClients()
    {
        $query = "SELECT idcliente, email, nome, cognome, città FROM cliente WHERE attivo=1 GROUP BY email, nome";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getClientsOfEvent($idevento)
    {
        //$query = "SELECT cliente FROM cliente_partecipa WHERE evento = ?";
        $query = "SELECT email FROM cliente, cliente_partecipa WHERE evento = ? AND idcliente = cliente";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $idevento);
        $stmt->execute();
        $result = $stmt->get_result();
        /*if ($result->fetch_all(MYSQLI_ASSOC) > 0){

        }*/
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function deleteOrganizer($organizzatore)
    {
        $query = "UPDATE organizzatore SET attivo = 0 WHERE idorganizzatore = ? AND attivo = 1";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $organizzatore);
        $stmt->execute();
        var_dump($stmt->error);
        return true;
    }

    public function deleteClient($cliente)
    {
        $query = "UPDATE cliente SET attivo = 0 WHERE idcliente = ? AND attivo = 1";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $cliente);
        $stmt->execute();
        var_dump($stmt->error);
        return true;
    }

    public function buyEvent($idcliente, $idevento, $num)
    {
        $query = "SELECT numbiglietti FROM cliente_partecipa WHERE cliente=? AND evento=?";
        $stmt = $this->db->prepare($query);
        $n = 0;
        $stmt->bind_param('ii', $idcliente, $idevento);
        $stmt->execute();
        $result = $stmt->get_result();
        $res = $result->fetch_all(MYSQLI_ASSOC);
        if (count($res) > 0) {
            $n = $res[0]["numbiglietti"];
            $query = "UPDATE cliente_partecipa SET numbiglietti=? WHERE cliente=? AND evento=?";
            $stmt = $this->db->prepare($query);
            $tot = $num + $n;
            $stmt->bind_param('iii', $tot, $idcliente, $idevento);
            $stmt->execute();
            var_dump($stmt->error);
            return true;
        }
        $query = "INSERT INTO cliente_partecipa (numbiglietti, cliente, evento) VALUES (?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('iii', $num, $idcliente, $idevento);
        $stmt->execute();
        var_dump($stmt->error);
        return true;
    }

    function emailDuplicated($email)
    {
        $people = array(
            "admin" => "email",
            "cliente" => "email",
            "organizzatore" => "email"
        );
        foreach ($people as $key => $value) {
            if ($res = $this->db->prepare("SELECT $value FROM $key WHERE $value = ?")) {
                $res->bind_param('s', $email);
                $res->execute();
                $res->store_result();
                $res->bind_result($id);
                $res->fetch();
                if (!is_null($id)) {
                    return true;
                }
            }
        }
        return false;
    }

    public function getNotifications($email)
    {
        $query = "SELECT * FROM notifica WHERE destinatario = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $email);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function viewNotification($id, $email)
    {
        $query = "UPDATE notifica SET visto = 1 WHERE id = ? AND destinatario = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('is', $id, $email);
        $stmt->execute();
        var_dump($stmt->error);
        return true;
    }

    public function deleteNotification($id, $email)
    {
        $query = "DELETE FROM notifica WHERE id = ? AND destinatario = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('is', $id, $email);
        $stmt->execute();
        var_dump($stmt->error);
        return true;
    }

    public function sendNotification($mittente, $destinatario, $testo)
    {
        $query = "INSERT INTO notifica (destinatario, messaggio, visto, mittente) VALUES (?, ?, 0, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sss', $destinatario, $testo, $mittente);
        $stmt->execute();
        var_dump($stmt->error);
        return true;
    }

    public function getOrganizer($idevento)
    {
        $query = "SELECT organizzatore FROM evento WHERE idevento = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $idevento);
        $stmt->execute();
        $result = $stmt->get_result();
        $id = $result->fetch_all(MYSQLI_ASSOC);
        $id = $id[0]["organizzatore"];
        $query = "SELECT email FROM organizzatore WHERE idorganizzatore = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function checkLogin($email, $password)
    {
        $query = "SELECT nome, email FROM admin WHERE email = ? AND password = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss', $email, $password);
        $stmt->execute();
        $result = $stmt->get_result();
        $ret = $result->fetch_all(MYSQLI_ASSOC);
        if (isset($ret[0]["nome"])) {
            $ret[0]["usertype"] = 0;
            return $ret;
        }
        $query = "SELECT idorganizzatore, nome, email FROM organizzatore WHERE attivo=1 AND email = ? AND password = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss', $email, $password);
        $stmt->execute();
        $result = $stmt->get_result();
        $ret = $result->fetch_all(MYSQLI_ASSOC);
        if (isset($ret[0]["idorganizzatore"])) {
            $ret[0]["usertype"] = 1;
            return $ret;
        }
        $query = "SELECT idcliente, nome, email FROM cliente WHERE attivo=1 AND email = ? AND password = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss', $email, $password);
        $stmt->execute();
        $result = $stmt->get_result();
        $ret = $result->fetch_all(MYSQLI_ASSOC);
        if (isset($ret[0]["idcliente"])) {
            $ret[0]["usertype"] = 2;
            return $ret;
        }
    }
}
