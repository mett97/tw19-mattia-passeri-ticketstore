<?php
require_once 'bootstrap.php';

if (isset($_SESSION["email"])) {
    $templateParams["notifiche"] = $dbh->getNotifications($_SESSION["email"]);
    for ($j = count($templateParams["notifiche"]) - 1; $j > 0; $j--) {
        for ($i = 0; $i < $j; $i++) {
            if ($templateParams["notifiche"][$i]["id"] < $templateParams["notifiche"][$i + 1]["id"]) {
                $temp = $templateParams["notifiche"][$i];
                $templateParams["notifiche"][$i] = $templateParams["notifiche"][$i + 1];
                $templateParams["notifiche"][$i + 1] = $temp;
            }
        }
    }
}

if (isset($_POST["email"]) && isset($_POST["password"])) {
    $_GET["formmsg"] = "";
    $pwd = hash("sha256", $_POST["password"]);
    $login_result = $dbh->checkLogin($_POST["email"], $pwd);
    if (empty($login_result) || count($login_result) == 0) {
        //Login fallito
        $templateParams["errorelogin"] = "Controllare email o password!";
    } else {
        if ($login_result[0]["usertype"] == 0) {
            registerLoggedAdmin($login_result[0]);
        } else if ($login_result[0]["usertype"] == 1) {
            registerLoggedOrganizer($login_result[0]);
        } else if ($login_result[0]["usertype"] == 2) {
            registerLoggedUser($login_result[0]);
        }
        if (isset($_SESSION["email"])) {
            $templateParams["notifiche"] = $dbh->getNotifications($_SESSION["email"]);
            for ($j = count($templateParams["notifiche"]) - 1; $j > 0; $j--) {
                for ($i = 0; $i < $j; $i++) {
                    if ($templateParams["notifiche"][$i]["id"] < $templateParams["notifiche"][$i + 1]["id"]) {
                        $temp = $templateParams["notifiche"][$i];
                        $templateParams["notifiche"][$i] = $templateParams["notifiche"][$i + 1];
                        $templateParams["notifiche"][$i + 1] = $temp;
                    }
                }
            }
        }
    }
}

if (isAdminLoggedIn()) {
    if (isset($_GET["page"]) && $_GET["page"] == 1) {
        $templateParams["titolo"] = "TicketStore - Admin";
        $templateParams["nome"] = "login/admin/login-organizzatori-admin.php";
        $templateParams["organizzatori"] = $dbh->getOrganizers();
        if (isset($_GET["formmsg"])) {
            $templateParams["formmsg"] = $_GET["formmsg"];
        }
    } else if (isset($_GET["page"]) && $_GET["page"] == 2) {
        $templateParams["titolo"] = "TicketStore - Admin";
        $templateParams["nome"] = "login/admin/login-clienti-admin.php";
        $templateParams["clienti"] = $dbh->getClients();
        if (isset($_GET["formmsg"])) {
            $templateParams["formmsg"] = $_GET["formmsg"];
        }
    } else if (isset($_GET["page"]) && $_GET["page"] == 3) {
        $templateParams["titolo"] = "TicketStore - Admin";
        $templateParams["nome"] = "login/admin/login-notifiche-admin.php";
        if (isset($_GET["formmsg"])) {
            $templateParams["formmsg"] = $_GET["formmsg"];
        }
    } else {
        $templateParams["titolo"] = "TicketStore - Admin";
        $templateParams["nome"] = "login/admin/login-home-admin.php";
        $templateParams["eventi"] = $dbh->getEvents();
        if (isset($_GET["formmsg"])) {
            $templateParams["formmsg"] = $_GET["formmsg"];
        }
    }
} else if (isOrganizerLoggedIn()) {
    if (isset($_GET["page"]) && $_GET["page"] == 1) {
        $templateParams["titolo"] = "TicketStore - Organizzatore";
        $templateParams["nome"] = "login/organizzatore/login-notifiche-organizzatore.php";
        if (isset($_GET["formmsg"])) {
            $templateParams["formmsg"] = $_GET["formmsg"];
        }
    } else {
        $templateParams["titolo"] = "TicketStore - Organizzatore";
        $templateParams["nome"] = "login/organizzatore/login-home-organizzatore.php";
        $templateParams["eventi"] = $dbh->getEventByOrganizerId($_SESSION["idorganizzatore"]);
        if (isset($_GET["formmsg"])) {
            $templateParams["formmsg"] = $_GET["formmsg"];
        }
    }
} else if (isClientLoggedIn()) {
    if (isset($_GET["page"]) && $_GET["page"] == 1) {
        $templateParams["titolo"] = "TicketStore - Cliente";
        $templateParams["nome"] = "login/cliente/login-notifiche-cliente.php";
        if (isset($_GET["formmsg"])) {
            $templateParams["formmsg"] = $_GET["formmsg"];
        }
    } else {
        $templateParams["titolo"] = "TicketStore - Cliente";
        $templateParams["nome"] = "login/cliente/login-home-cliente.php";
        $templateParams["eventi"] = $dbh->getEventByClientId($_SESSION["idcliente"]);
        if (isset($_GET["formmsg"])) {
            $templateParams["formmsg"] = $_GET["formmsg"];
        }
    }
} else {
    $templateParams["titolo"] = "TicketStore - Login";
    $templateParams["nome"] = "login-form.php";
}

require 'template/base.php';
?>