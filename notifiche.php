<?php
require_once 'bootstrap.php';

if(isset($_SESSION["email"])){
    $templateParams["notifiche"] = $dbh->getNotifications($_SESSION["email"]);
}

if(isAdminLoggedIn()){
    if(isset($_POST["visto"])){
        $id = intval($_POST["id"]);
        $email = $_SESSION["email"];
        $res = $dbh->viewNotification($id, $email);
        if($res){
            $msg = "Notifica letta!";
        }else{
            $msg = "Impossibile leggere la notifica!";
        }
    }elseif(isset($_POST["cancella"])){
        $id = $_POST["id"];
        $email = $_SESSION["email"];
        $res = $dbh->deleteNotification($id, $email);
        if($res){
            $msg = "Notifica eliminata!";
        }else{
            $msg = "Impossibile eliminare la notifica!";
        }
    }
    header("location: login.php?page=3&formmsg=".$msg);
}elseif(isOrganizerLoggedIn()){
    if(isset($_POST["visto"])){
        $id = intval($_POST["id"]);
        $email = $_SESSION["email"];
        $res = $dbh->viewNotification($id, $email);
        if($res){
            $msg = "Notifica letta!";
        }else{
            $msg = "Impossibile leggere la notifica!";
        }
    }elseif(isset($_POST["cancella"])){
        $id = $_POST["id"];
        $email = $_SESSION["email"];
        $res = $dbh->deleteNotification($id, $email);
        if($res){
            $msg = "Notifica eliminata!";
        }else{
            $msg = "Impossibile eliminare la notifica!";
        }
    }
    header("location: login.php?page=1&formmsg=".$msg);
}elseif(isClientLoggedIn()){
    if(isset($_POST["visto"])){
        $id = intval($_POST["id"]);
        $email = $_SESSION["email"];
        $res = $dbh->viewNotification($id, $email);
        if($res){
            $msg = "Notifica letta!";
        }else{
            $msg = "Impossibile leggere la notifica!";
        }
    }elseif(isset($_POST["cancella"])){
        $id = $_POST["id"];
        $email = $_SESSION["email"];
        $res = $dbh->deleteNotification($id, $email);
        if($res){
            $msg = "Notifica eliminata!";
        }else{
            $msg = "Impossibile eliminare la notifica!";
        }
    }
    header("location: login.php?page=1&formmsg=".$msg);
}else{
    $msg = "Impossibile processare notifiche!";
    header("location: login.php?formmsg=".$msg);
}

?>