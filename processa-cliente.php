<?php
require_once 'bootstrap.php';

if (isset($_SESSION["email"])) {
    $templateParams["notifiche"] = $dbh->getNotifications($_SESSION["email"]);
}

if ((!isAdminLoggedIn()) || !isset($_POST["action"])) {
    header("location: login.php");
}

if ($_POST["action"] == 1 && !isAdminLoggedIn() && !isOrganizerLoggedIn() && !isClientLoggedIn()) {
    //Inserisco
    $nome = $_POST["nome"];
    $cognome = $_POST["cognome"];
    $email = $_POST["email"];
    $res = $dbh->emailDuplicated($email);
    if ($res) {
        header('Location: /TicketStore/signup.php?ucat=cl&error=-1');
        exit;
    }
    $città = $_POST["città"];
    $password = $_POST["password"];
    $hashed = hash("sha256", $password);
    $id = $dbh->insertUser($nome, $cognome, $email, $hashed, $città);
    if ($id != false) {
        $msg = "Registrazione completata correttamente! Accedi ora.";
        $ads = $dbh->getAdmins();
        foreach ($ads as $ad) {
            $dbh->sendNotification($email, $ad["email"], "Un cliente si è registrato");
        }
    } else {
        $msg = "Errore in inserimento!";
    }
    header("location: login.php?formmsg=" . $msg);
}

if ($_POST["action"] == 3) {
    //cancello
    $idcliente = $_POST["idcliente"];
    $dbh->deleteClient($idcliente);
    $ads = $dbh->getAdmins();
    foreach ($ads as $ad) {
        $dbh->sendNotification($_SESSION["email"], $ad["email"], "Un cliente è stato cancellato");
    }
    $msg = "Cancellazione completata correttamente!";
    header("location: login.php?formmsg=" . $msg);
}
