<?php
require_once 'bootstrap.php';

if (isset($_SESSION["email"])) {
    $templateParams["notifiche"] = $dbh->getNotifications($_SESSION["email"]);
}

if ((!isOrganizerLoggedIn() && !isAdminLoggedIn()) || !isset($_POST["action"])) {
    header("location: login.php");
}

if ($_POST["action"] == 1) {
    //Inserisco
    $titoloevento = $_POST["titoloevento"];
    $testoevento = $_POST["testoevento"];
    $anteprimaevento = $_POST["anteprimaevento"];
    $dataevento = date("Y-m-d");
    $organizzatore = $_SESSION["idorganizzatore"];
    $dataevento = $_POST["dataevento"];
    $luogoevento = $_POST["luogoevento"];
    $maxbiglietti = $_POST["maxbiglietti"];
    $nomeevento = $dbh->getEventById($idevento)[0]["titoloevento"];
    $categorie = $dbh->getCategories();
    $categorie_inserite = array();
    foreach ($categorie as $categoria) {
        if (isset($_POST["categoria_" . $categoria["idcategoria"]])) {
            array_push($categorie_inserite, $categoria["idcategoria"]);
        }
    }

    list($result, $msg) = uploadImage(UPLOAD_DIR, $_FILES["imgevento"]);
    if ($result != 0) {
        $imgevento = $msg;
        $id = $dbh->insertEvent($titoloevento, $testoevento, $anteprimaevento, $luogoevento, $dataevento, $imgevento, $organizzatore, $maxbiglietti);
        if ($id != false) {
            foreach ($categorie_inserite as $categoria) {
                $ris = $dbh->insertCategoryOfEvent($id, $categoria);
            }
            $msg = "Inserimento completato correttamente!";
            $ads = $dbh->getAdmins();
            foreach ($ads as $ad) {
                $dbh->sendNotification($_SESSION["email"], $ad["email"], "Un organizzatore ha creato un suo evento ".$nomeevento);
            }
        } else {
            $msg = "Errore in inserimento!";
        }
    }
    header("location: login.php?formmsg=" . $msg);
}
if ($_POST["action"] == 2) {
    //modifico
    $idevento = intval($_POST["idevento"]);
    $titoloevento = $_POST["titoloevento"];
    $testoevento = $_POST["testoevento"];
    $anteprimaevento = $_POST["anteprimaevento"];
    $organizzatore = $_SESSION["idorganizzatore"];
    $dataevento = $_POST["dataevento"];
    $luogoevento = $_POST["luogoevento"];
    $maxbiglietti = intval($_POST["maxbiglietti"]);
    $nomeevento = $dbh->getEventById($idevento)[0]["titoloevento"];
    if (isset($_FILES["imgevento"]) && strlen($_FILES["imgevento"]["name"]) > 0) {
        list($result, $msg) = uploadImage(UPLOAD_DIR, $_FILES["imgevento"]);
        if ($result == 0) {
            header("location: login.php?formmsg=" . $msg);
        }
        $imgevento = $msg;
    } else {
        $imgevento = $_POST["oldimg"];
    }
    $dbh->updateEventOfOrganizer($idevento, $titoloevento, $testoevento, $anteprimaevento, $luogoevento, $dataevento, $imgevento, $organizzatore, $maxbiglietti);

    $categorie = $dbh->getCategories();
    $categorie_inserite = array();
    foreach ($categorie as $categoria) {
        if (isset($_POST["categoria_" . $categoria["idcategoria"]])) {
            array_push($categorie_inserite, $categoria["idcategoria"]);
        }
    }
    $categorievecchie = explode(",", $_POST["categorie"]);

    $categoriedaeliminare = array_diff($categorievecchie, $categorie_inserite);
    foreach ($categoriedaeliminare as $categoria) {
        $ris = $dbh->deleteCategoryOfEvent($idevento, $categoria);
    }
    $categoriedainserire = array_diff($categorie_inserite, $categorievecchie);
    foreach ($categoriedainserire as $categoria) {
        $ris = $dbh->insertCategoryOfEvent($idevento, $categoria);
    }
    $ads = $dbh->getAdmins();
    foreach ($ads as $ad) {
        $dbh->sendNotification($_SESSION["email"], $ad["email"], "Un organizzatore ha modificato il suo evento ".$nomeevento);
    }
    $cls = $dbh->getClientsOfEvent($idevento);
    if (!empty($cls)) {
        foreach ($cls as $cl) {
            $dbh->sendNotification($_SESSION["email"], $cl["email"], "Un organizzatore ha modificato il suo evento ".$nomeevento);
        }
    }
    $msg = "Modifica completata correttamente!";
    header("location: login.php?formmsg=" . $msg);
}

if ($_POST["action"] == 3) {
    //cancello
    $idevento = $_POST["idevento"];
    $nomeevento = $dbh->getEventById($idevento)[0]["titoloevento"];
    if (isOrganizerLoggedIn()) {
        $ads = $dbh->getAdmins();
        foreach ($ads as $ad) {
            $dbh->sendNotification($_SESSION["email"], $ad["email"], "Un organizzatore ha cancellato il suo evento ".$nomeevento);
        }
    } elseif (isAdminLoggedIn()) {
        $destinatario = $dbh->getOrganizer($idevento)[0]["email"];
        $dbh->sendNotification($_SESSION["email"], $destinatario, "Un admin ha cancellato il tuo evento ".$nomeevento);
    }
    $cls = $dbh->getClientsOfEvent($idevento);
    if (!empty($cls)) {
        foreach ($cls as $cl) {
            $dbh->sendNotification($_SESSION["email"], $cl["email"], "E' stato cancellato l'evento ".$nomeevento);
        }
    }
    $dbh->deleteCategoriesOfEvent($idevento);
    $dbh->deleteEvent($idevento);
    $msg = "Cancellazione completata correttamente!";
    header("location: login.php?formmsg=" . $msg);
}
